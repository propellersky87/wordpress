<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе установки.
 * Необязательно использовать веб-интерфейс, можно скопировать файл в "wp-config.php"
 * и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://ru.wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Параметры базы данных: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define( 'DB_NAME', 'wp' );

/** Имя пользователя базы данных */
define( 'DB_USER', 'root' );

/** Пароль к базе данных */
define( 'DB_PASSWORD', '' );

/** Имя сервера базы данных */
define( 'DB_HOST', 'localhost' );

/** Кодировка базы данных для создания таблиц. */
define( 'DB_CHARSET', 'utf8mb4' );

/** Схема сопоставления. Не меняйте, если не уверены. */
define( 'DB_COLLATE', '' );

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу. Можно сгенерировать их с помощью
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}.
 *
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными.
 * Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'QKa]A$s|3Dn_.vBoAOr4I2j8xTr)t>$04Ko>uPf2j]<`D>t>[<u`=Mf~NK5vP8N>' );
define( 'SECURE_AUTH_KEY',  'zB5:-^N$#xj4S/v*UIZus$m4@$X%`{7$j}J?$WU906coz/,fhE}=!;!(pl&GGdt0' );
define( 'LOGGED_IN_KEY',    'Y>vohwL)b<e0?YI[^rrMZSN`Lgv}fsJD<O&Fx{3!*@8%58NS!Tn:[E<LP]H6tT`]' );
define( 'NONCE_KEY',        ')]Y~<JpH8049u6!n/MAB2&# JwL%IZoQ $M S2Gflim`Zx[pOsN-q?I5UYw_fn/m' );
define( 'AUTH_SALT',        'VYeIk8 X[4#[cJvuL#4{q5aCdiLRgo_<.<G2{B-LRp21mSbPtf.RED7wDy(@}S0u' );
define( 'SECURE_AUTH_SALT', '.[rQmoiIei6;BU/@]rwbHf)-!]gR4p/M+-k%hS82l<n71!e:%BG[:kli`f  i&0j' );
define( 'LOGGED_IN_SALT',   'X16~v[ZG-@yfac z-t(iGh%/()O ^Z)pE5/Tq6@0hU2#_>D$D?&<#^$ANm<O~LeV' );
define( 'NONCE_SALT',       '@Me}iGAioTu AR~ bPh;o+])|M7gHq zb-,neT|=qm}5 -jiDx=75eeq=mJ@BHY,' );

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix = 'wp_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 *
 * Информацию о других отладочных константах можно найти в документации.
 *
 * @link https://ru.wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Произвольные значения добавляйте между этой строкой и надписью "дальше не редактируем". */



/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Инициализирует переменные WordPress и подключает файлы. */
require_once ABSPATH . 'wp-settings.php';
